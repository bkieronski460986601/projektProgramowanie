﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Threading;

namespace projekt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

         /// </summary>
         ///Inicjowanie parametrów
         /// </summary>
    static int[,] grid = new int[9, 9];
        static string s;
        static void Init(ref int[,] grid)
        {
        /// </summary>
        ///Inicjowanie siatki do sudoku
        /// </summary>
        for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    grid[i, j] = (i * 3 + i / 3 + j) % 9 + 1;
                }
            }
        }
        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            /// </summary>
            ///Zmiana przyjmowanych argumentów do text boxów na tylko liczby
            /// </summary>
            Regex regex = new Regex("[^0-9]+");
             e.Handled = regex.IsMatch(e.Text);
        }
         static void Draw(ref int[,] grid, out string _s)
         {
            /// </summary>
            ///zapis wygenerowanego sudoku do pliku
            /// </summary>
            for (int x = 0; x < 9; x++)
                {
                    for (int y = 0; y < 9; y++)
                    {
                        s += grid[x, y].ToString();
                        System.IO.File.WriteAllText("sudokuPelne.txt", s);
                    }
                }
                _s = s;
                s = "";

            }
        static void ChangeTwoCell(ref int[,] grid, int findValue1, int findValue2)
            {
            /// </summary>
            ///Generowanie działanej siatki sudoku
            /// </summary>
            int xParm1, yParm1, xParm2, yParm2;
                xParm1 = yParm1 = xParm2 = yParm2 = 0;
                for (int i = 0; i < 9; i += 3)
                {
                    for (int k = 0; k < 9; k += 3)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            for (int z = 0; z < 3; z++)
                            {
                                if (grid[i + j, k + z] == findValue1)
                                {
                                    xParm1 = i + j;
                                    yParm1 = k + z;

                                }
                                if (grid[i + j, k + z] == findValue2)
                                {
                                    xParm2 = i + j;
                                    yParm2 = k + z;

                                }
                            }
                        }
                        grid[xParm1, yParm1] = findValue2;
                        grid[xParm2, yParm2] = findValue1;
                    }
                }
            }
            static void Update(ref int[,] grid, int shuffleLevel)
            {
            /// </summary>
            ///Randomizacja pierwszej lini sudoku
            /// </summary>
            for (int repeat = 0; repeat < shuffleLevel; repeat++)
            {
                Random rand = new Random(Guid.NewGuid().GetHashCode());
                Random rand2 = new Random(Guid.NewGuid().GetHashCode());
                ChangeTwoCell(ref grid, rand.Next(1, 9), rand2.Next(1, 9));
            }
            }
        public MainWindow()
        {
            InitializeComponent();
        }
            private void sudoku(out string _s)
            {
            /// </summary>
            ///Przygotowywanie liczb do sudoku do wstawienia do text boxów zgodnie z wybranym poziomem trudności
            /// </summary>
            if (rbTrudny.IsChecked == true)
                {
                    string numbers = File.ReadAllText(@"SudokuPelne.txt");
                    var nm = numbers;

                    char[] k = nm.ToCharArray();

                    int[] randomized = new int[70];

                    for (int repeat = 0; repeat < 70; repeat++)
                    {
                        Random randNum = new Random(Guid.NewGuid().GetHashCode());
                        randomized[repeat] = randNum.Next(0, 80);
                    }
                    int f;
                    string b = "0";
                    for (int y = 0; y < 70; y++)
                    {

                        Random random = new Random(Guid.NewGuid().GetHashCode()); ;
                        f = random.Next(0, 81);
                        k[f] = Convert.ToChar(b);

                    }
                    for (int w = 0; w < 81; w++)
                    {
                        s += k[w].ToString();
                        System.IO.File.WriteAllText("sudokupuste.txt", s);
                    }

                }
                else if (rbSredni.IsChecked == true)
                {
                    string numbers = File.ReadAllText(@"SudokuPelne.txt");
                    var nm = numbers;

                    char[] k = nm.ToCharArray();

                    int[] randomized = new int[50];

                    for (int repeat = 0; repeat < 50; repeat++)
                    {
                        Random randNum = new Random(Guid.NewGuid().GetHashCode());
                        randomized[repeat] = randNum.Next(0, 80);
                    }
                    int f;
                    string b = "0";
                    for (int y = 0; y < 50; y++)
                    {

                        Random random = new Random(Guid.NewGuid().GetHashCode()); ;
                        f = random.Next(0, 81);
                        k[f] = Convert.ToChar(b);

                    }
                    for (int w = 0; w < 81; w++)
                    {
                        s += k[w].ToString();
                        System.IO.File.WriteAllText("sudokupuste.txt", s);
                    }
                }
                else if (rbLatwy.IsChecked == true)
                {
                    string numbers = File.ReadAllText(@"SudokuPelne.txt");
                    var nm = numbers;

                    char[] k = nm.ToCharArray();

                    int[] randomized = new int[30];

                    for (int repeat = 0; repeat < 30; repeat++)
                    {
                        Random randNum = new Random(Guid.NewGuid().GetHashCode());
                        randomized[repeat] = randNum.Next(0, 80);
                    }
                    int f;
                    string b = "0";
                    for (int y = 0; y < 30; y++)
                    {

                        Random random = new Random(Guid.NewGuid().GetHashCode()); ;
                        f = random.Next(0, 81);
                        k[f] = Convert.ToChar(b);

                    }
                    for (int w = 0; w < 81; w++)
                    {
                        s += k[w].ToString();
                        System.IO.File.WriteAllText("sudokupuste.txt", s);
                    }
                }
                else
                {
                    MessageBox.Show("Wybierz poziom trudnosci!");
                }
                _s = s;
                s = "";
            }
            private void clear()
            {
            /// </summary>
            ///Funkcja czyszcząca text boxy
            /// </summary>

            tb11.Text = String.Empty;
                tb11.IsReadOnly = false;

                tb12.Text = String.Empty;
                tb12.IsReadOnly = false;

                tb13.Text = String.Empty;
                tb13.IsReadOnly = false;

                tb14.IsReadOnly = false;
                tb14.Text = String.Empty;

                tb15.IsReadOnly = false;
                tb15.Text = String.Empty;

                tb16.IsReadOnly = false;
                tb16.Text = String.Empty;

                tb17.IsReadOnly = false;
                tb17.Text = String.Empty;

                tb18.IsReadOnly = false;
                tb18.Text = String.Empty;

                tb19.IsReadOnly = false;
                tb19.Text = String.Empty;

                tb21.Text = String.Empty;
                tb21.IsReadOnly = false;

                tb22.Text = String.Empty;
                tb22.IsReadOnly = false;

                tb23.Text = String.Empty;
                tb23.IsReadOnly = false;

                tb24.Text = String.Empty;
                tb24.IsReadOnly = false;

                tb25.IsReadOnly = false;
                tb25.Text = String.Empty;

                tb26.IsReadOnly = false;
                tb26.Text = String.Empty;

                tb27.IsReadOnly = false;
                tb27.Text = String.Empty;

                tb28.IsReadOnly = false;
                tb28.Text = String.Empty;

                tb29.IsReadOnly = false;
                tb29.Text = String.Empty;

                tb31.IsReadOnly = false;
                tb31.Text = String.Empty;

                tb32.IsReadOnly = false;
                tb32.Text = String.Empty;

                tb33.IsReadOnly = false;
                tb33.Text = String.Empty;

                tb34.IsReadOnly = false;
                tb34.Text = String.Empty;

                tb35.IsReadOnly = false;
                tb35.Text = String.Empty;

                tb36.IsReadOnly = false;
                tb36.Text = String.Empty;

                tb37.IsReadOnly = false;
                tb37.Text = String.Empty;

                tb38.IsReadOnly = false;
                tb38.Text = String.Empty;

                tb39.IsReadOnly = false;
                tb39.Text = String.Empty;

                tb41.IsReadOnly = false;
                tb41.Text = String.Empty;

                tb42.IsReadOnly = false;
                tb42.Text = String.Empty;

                tb43.IsReadOnly = false;
                tb43.Text = String.Empty;

                tb44.IsReadOnly = false;
                tb44.Text = String.Empty;

                tb45.IsReadOnly = false;
                tb45.Text = String.Empty;

                tb46.IsReadOnly = false;
                tb46.Text = String.Empty;

                tb47.IsReadOnly = false;
                tb47.Text = String.Empty;

                tb48.IsReadOnly = false;
                tb48.Text = String.Empty;

                tb49.IsReadOnly = false;
                tb49.Text = String.Empty;

                tb51.IsReadOnly = false;
                tb51.Text = String.Empty;

                tb52.IsReadOnly = false;
                tb52.Text = String.Empty;

                tb53.IsReadOnly = false;
                tb53.Text = String.Empty;

                tb54.IsReadOnly = false;
                tb54.Text = String.Empty;

                tb55.IsReadOnly = false;
                tb55.Text = String.Empty;

                tb56.IsReadOnly = false;
                tb56.Text = String.Empty;

                tb57.IsReadOnly = false;
                tb57.Text = String.Empty;

                tb58.IsReadOnly = false;
                tb58.Text = String.Empty;

                tb59.IsReadOnly = false;
                tb59.Text = String.Empty;

                tb61.IsReadOnly = false;
                tb61.Text = String.Empty;

                tb62.IsReadOnly = false;
                tb62.Text = String.Empty;

                tb63.IsReadOnly = false;
                tb63.Text = String.Empty;

                tb64.IsReadOnly = false;
                tb64.Text = String.Empty;

                tb65.IsReadOnly = false;
                tb65.Text = String.Empty;

                tb66.IsReadOnly = false;
                tb66.Text = String.Empty;

                tb67.IsReadOnly = false;
                tb67.Text = String.Empty;

                tb68.IsReadOnly = false;
                tb68.Text = String.Empty;

                tb69.IsReadOnly = false;
                tb69.Text = String.Empty;

                tb71.IsReadOnly = false;
                tb71.Text = String.Empty;

                tb72.IsReadOnly = false;
                tb72.Text = String.Empty;

                tb73.IsReadOnly = false;
                tb73.Text = String.Empty;

                tb74.IsReadOnly = false;
                tb74.Text = String.Empty;

                tb75.IsReadOnly = false;
                tb75.Text = String.Empty;

                tb76.IsReadOnly = false;
                tb76.Text = String.Empty;

                tb77.IsReadOnly = false;
                tb77.Text = String.Empty;

                tb78.IsReadOnly = false;
                tb78.Text = String.Empty;

                tb79.IsReadOnly = false;
                tb79.Text = String.Empty;

                tb77.IsReadOnly = false;
                tb77.Text = String.Empty;

                tb78.IsReadOnly = false;
                tb78.Text = String.Empty;

                tb79.IsReadOnly = false;
                tb79.Text = String.Empty;

                tb81.IsReadOnly = false;
                tb81.Text = String.Empty;

                tb82.IsReadOnly = false;
                tb82.Text = String.Empty;

                tb83.IsReadOnly = false;
                tb83.Text = String.Empty;

                tb84.IsReadOnly = false;
                tb84.Text = String.Empty;

                tb85.IsReadOnly = false;
                tb85.Text = String.Empty;

                tb86.IsReadOnly = false;
                tb86.Text = String.Empty;

                tb87.IsReadOnly = false;
                tb87.Text = String.Empty;

                tb88.IsReadOnly = false;
                tb88.Text = String.Empty;

                tb89.IsReadOnly = false;
                tb89.Text = String.Empty;

                tb91.IsReadOnly = false;
                tb91.Text = String.Empty;

                tb92.IsReadOnly = false;
                tb92.Text = String.Empty;

                tb93.IsReadOnly = false;
                tb93.Text = String.Empty;

                tb94.IsReadOnly = false;
                tb94.Text = String.Empty;

                tb95.IsReadOnly = false;
                tb95.Text = String.Empty;

                tb96.IsReadOnly = false;
                tb96.Text = String.Empty;

                tb97.IsReadOnly = false;
                tb97.Text = String.Empty;

                tb98.IsReadOnly = false;
                tb98.Text = String.Empty;

                tb99.IsReadOnly = false;
                tb99.Text = String.Empty;
            }
        private void wygrana()
        {
            if (File.Exists(@"sudokuPelne.txt"))
            {
                /// </summary>
                ///Sprawdzenie warunków wygrania
                /// </summary>
                string line1 = File.ReadAllText(@"sudokuPelne.txt");
                var ln = line1;
                char[] s = ln.ToCharArray();

                if ((s[0].ToString() == tb11.Text) &&
                   (s[1].ToString() == tb12.Text) &&
                   (s[2].ToString() == tb13.Text) &&
                   (s[3].ToString() == tb14.Text) &&
                   (s[4].ToString() == tb15.Text) &&
                   (s[5].ToString() == tb16.Text) &&
                   (s[6].ToString() == tb17.Text) &&
                   (s[7].ToString() == tb18.Text) &&
                   (s[8].ToString() == tb19.Text) &&
                   (s[9].ToString() == tb21.Text) &&
                   (s[10].ToString() == tb22.Text) &&
                   (s[11].ToString() == tb23.Text) &&
                   (s[12].ToString() == tb24.Text) &&
                   (s[13].ToString() == tb25.Text) &&
                   (s[14].ToString() == tb26.Text) &&
                   (s[15].ToString() == tb27.Text) &&
                   (s[16].ToString() == tb28.Text) &&
                   (s[17].ToString() == tb29.Text) &&
                   (s[18].ToString() == tb31.Text) &&
                   (s[19].ToString() == tb32.Text) &&
                   (s[20].ToString() == tb33.Text) &&
                   (s[21].ToString() == tb34.Text) &&
                   (s[22].ToString() == tb35.Text) &&
                   (s[23].ToString() == tb36.Text) &&
                   (s[24].ToString() == tb37.Text) &&
                   (s[25].ToString() == tb38.Text) &&
                   (s[26].ToString() == tb39.Text) &&
                   (s[27].ToString() == tb41.Text) &&
                   (s[28].ToString() == tb42.Text) &&
                   (s[29].ToString() == tb43.Text) &&
                   (s[30].ToString() == tb44.Text) &&
                   (s[31].ToString() == tb45.Text) &&
                   (s[32].ToString() == tb46.Text) &&
                   (s[33].ToString() == tb47.Text) &&
                   (s[34].ToString() == tb48.Text) &&
                   (s[35].ToString() == tb49.Text) &&
                   (s[36].ToString() == tb51.Text) &&
                   (s[37].ToString() == tb52.Text) &&
                   (s[38].ToString() == tb53.Text) &&
                   (s[39].ToString() == tb54.Text) &&
                   (s[40].ToString() == tb55.Text) &&
                   (s[41].ToString() == tb56.Text) &&
                   (s[42].ToString() == tb57.Text) &&
                   (s[43].ToString() == tb58.Text) &&
                   (s[44].ToString() == tb59.Text) &&
                   (s[45].ToString() == tb61.Text) &&
                   (s[46].ToString() == tb62.Text) &&
                   (s[47].ToString() == tb63.Text) &&
                   (s[48].ToString() == tb64.Text) &&
                   (s[49].ToString() == tb65.Text) &&
                   (s[50].ToString() == tb66.Text) &&
                   (s[51].ToString() == tb67.Text) &&
                   (s[52].ToString() == tb68.Text) &&
                   (s[53].ToString() == tb69.Text) &&
                   (s[54].ToString() == tb71.Text) &&
                   (s[55].ToString() == tb72.Text) &&
                   (s[56].ToString() == tb73.Text) &&
                   (s[57].ToString() == tb74.Text) &&
                   (s[58].ToString() == tb75.Text) &&
                   (s[59].ToString() == tb76.Text) &&
                   (s[60].ToString() == tb77.Text) &&
                   (s[61].ToString() == tb78.Text) &&
                   (s[62].ToString() == tb79.Text) &&
                   (s[63].ToString() == tb81.Text) &&
                   (s[64].ToString() == tb82.Text) &&
                   (s[65].ToString() == tb83.Text) &&
                   (s[66].ToString() == tb84.Text) &&
                   (s[67].ToString() == tb85.Text) &&
                   (s[68].ToString() == tb86.Text) &&
                   (s[69].ToString() == tb87.Text) &&
                   (s[70].ToString() == tb88.Text) &&
                   (s[71].ToString() == tb89.Text) &&
                   (s[72].ToString() == tb91.Text) &&
                   (s[73].ToString() == tb92.Text) &&
                   (s[74].ToString() == tb93.Text) &&
                   (s[75].ToString() == tb94.Text) &&
                   (s[76].ToString() == tb95.Text) &&
                   (s[77].ToString() == tb96.Text) &&
                   (s[78].ToString() == tb97.Text) &&
                   (s[79].ToString() == tb98.Text) &&
                   (s[80].ToString() == tb99.Text))
                {
                    MessageBox.Show("Gratulacje wygałeś");
                    File.Delete("sudokuPelne.txt");
                    File.Delete("SudokuPuste.txt");
                }
                /// </summary>
                ///Sprawdzenie popełnienia blędu w rozwiązaniu
                /// </summary>
                else if (((s[0].ToString() != tb11.Text) ||
                   (s[1].ToString() != tb12.Text) ||
                   (s[2].ToString() != tb13.Text) ||
                   (s[3].ToString() != tb14.Text) ||
                   (s[4].ToString() != tb15.Text) ||
                   (s[5].ToString() != tb16.Text) ||
                   (s[6].ToString() != tb17.Text) ||
                   (s[7].ToString() != tb18.Text) ||
                   (s[8].ToString() != tb19.Text) ||
                   (s[9].ToString() != tb21.Text) ||
                   (s[10].ToString() != tb22.Text) ||
                   (s[11].ToString() != tb23.Text) ||
                   (s[12].ToString() != tb24.Text) ||
                   (s[13].ToString() != tb25.Text) ||
                   (s[14].ToString() != tb26.Text) ||
                   (s[15].ToString() != tb27.Text) ||
                   (s[16].ToString() != tb28.Text) ||
                   (s[17].ToString() != tb29.Text) ||
                   (s[18].ToString() != tb31.Text) ||
                   (s[19].ToString() != tb32.Text) ||
                   (s[20].ToString() != tb33.Text) ||
                   (s[21].ToString() != tb34.Text) ||
                   (s[22].ToString() != tb35.Text) ||
                   (s[23].ToString() != tb36.Text) ||
                   (s[24].ToString() != tb37.Text) ||
                   (s[25].ToString() != tb38.Text) ||
                   (s[26].ToString() != tb39.Text) ||
                   (s[27].ToString() != tb41.Text) ||
                   (s[28].ToString() != tb42.Text) ||
                   (s[29].ToString() != tb43.Text) ||
                   (s[30].ToString() != tb44.Text) ||
                   (s[31].ToString() != tb45.Text) ||
                   (s[32].ToString() != tb46.Text) ||
                   (s[33].ToString() != tb47.Text) ||
                   (s[34].ToString() != tb48.Text) ||
                   (s[35].ToString() != tb49.Text) ||
                   (s[36].ToString() != tb51.Text) ||
                   (s[37].ToString() != tb52.Text) ||
                   (s[38].ToString() != tb53.Text) ||
                   (s[39].ToString() != tb54.Text) ||
                   (s[40].ToString() != tb55.Text) ||
                   (s[41].ToString() != tb56.Text) ||
                   (s[42].ToString() != tb57.Text) ||
                   (s[43].ToString() != tb58.Text) ||
                   (s[44].ToString() != tb59.Text) ||
                   (s[45].ToString() != tb61.Text) ||
                   (s[46].ToString() != tb62.Text) ||
                   (s[47].ToString() != tb63.Text) ||
                   (s[48].ToString() != tb64.Text) ||
                   (s[49].ToString() != tb65.Text) ||
                   (s[50].ToString() != tb66.Text) ||
                   (s[51].ToString() != tb67.Text) ||
                   (s[52].ToString() != tb68.Text) ||
                   (s[53].ToString() != tb69.Text) ||
                   (s[54].ToString() != tb71.Text) ||
                   (s[55].ToString() != tb72.Text) ||
                   (s[56].ToString() != tb73.Text) ||
                   (s[57].ToString() != tb74.Text) ||
                   (s[58].ToString() != tb75.Text) ||
                   (s[59].ToString() != tb76.Text) ||
                   (s[60].ToString() != tb77.Text) ||
                   (s[61].ToString() != tb78.Text) ||
                   (s[62].ToString() != tb79.Text) ||
                   (s[63].ToString() != tb81.Text) ||
                   (s[64].ToString() != tb82.Text) ||
                   (s[65].ToString() != tb83.Text) ||
                   (s[66].ToString() != tb84.Text) ||
                   (s[67].ToString() != tb85.Text) ||
                   (s[68].ToString() != tb86.Text) ||
                   (s[69].ToString() != tb87.Text) ||
                   (s[70].ToString() != tb88.Text) ||
                   (s[71].ToString() != tb89.Text) ||
                   (s[72].ToString() != tb91.Text) ||
                   (s[73].ToString() != tb92.Text) ||
                   (s[74].ToString() != tb93.Text) ||
                   (s[75].ToString() != tb94.Text) ||
                   (s[76].ToString() != tb95.Text) ||
                   (s[77].ToString() != tb96.Text) ||
                   (s[78].ToString() != tb97.Text) ||
                   (s[79].ToString() != tb98.Text) ||
                   (s[80].ToString() != tb99.Text)))
                {
                    MessageBox.Show("Masz gdzieś błąd");
                }

            }
            else
            {
                MessageBox.Show("Rozpocznij Grę");
            }
        }
        private void btnPlay_Click(object sender, RoutedEventArgs e)
            {
            /// </summary>
            ///Przycisk rozpoczynający rozgrywkę
             /// </summary>
            string crit;
            Init(ref grid);
            Update(ref grid, 10);
            Draw(ref grid, out crit);

            sudoku(out crit);
            if (File.Exists("sudokuPuste.txt"))
            {
                string line1 = File.ReadAllText(@"sudokuPuste.txt");
                var ln = line1;
                char[] s = ln.ToCharArray();

                /// </summary>
                ///Wyświetlanie liczb do text boxów
                /// </summary>

                clear();

                if (s[0] != '0')
                {
                    tb11.IsReadOnly = true;
                    tb11.Text = s[0].ToString();
                }
                if (s[1] != '0')
                {
                    tb12.IsReadOnly = true;
                    tb12.Text = s[1].ToString();
                }
                if (s[2] != '0')
                {
                    tb13.IsReadOnly = true;
                    tb13.Text = s[2].ToString();
                }
                if (s[3] != '0')
                {
                    tb14.IsReadOnly = true;
                    tb14.Text = s[3].ToString();
                }
                if (s[4] != '0')
                {
                    tb15.IsReadOnly = true;
                    tb15.Text = s[4].ToString();
                }
                if (s[5] != '0')
                {
                    tb16.IsReadOnly = true;
                    tb16.Text = s[5].ToString();
                }
                if (s[6] != '0')
                {
                    tb17.IsReadOnly = true;
                    tb17.Text = s[6].ToString();
                }
                if (s[7] != '0')
                {
                    tb18.IsReadOnly = true;
                    tb18.Text = s[7].ToString();
                }
                if (s[8] != '0')
                {
                    tb19.IsReadOnly = true;
                    tb19.Text = s[8].ToString();
                }
                if (s[9] != '0')
                {
                    tb21.IsReadOnly = true;
                    tb21.Text = s[9].ToString();
                }
                if (s[10] != '0')
                {
                    tb22.IsReadOnly = true;
                    tb22.Text = s[10].ToString();

                }
                if (s[11] != '0')
                {
                    tb23.IsReadOnly = true;
                    tb23.Text = s[11].ToString();
                }
                if (s[12] != '0')
                {
                    tb24.IsReadOnly = true;
                    tb24.Text = s[12].ToString();
                }
                if (s[13] != '0')
                {
                    tb25.IsReadOnly = true;
                    tb25.Text = s[13].ToString();
                }
                if (s[14] != '0')
                {
                    tb26.IsReadOnly = true;
                    tb26.Text = s[14].ToString();
                }
                if (s[15] != '0')
                {
                    tb27.IsReadOnly = true;
                    tb27.Text = s[15].ToString();
                }
                if (s[16] != '0')
                {
                    tb28.IsReadOnly = true;
                    tb28.Text = s[16].ToString();
                }
                if (s[17] != '0')
                {
                    tb29.IsReadOnly = true;
                    tb29.Text = s[17].ToString();
                }
                if (s[18] != '0')
                {
                    tb31.IsReadOnly = true;
                    tb31.Text = s[18].ToString();
                }
                if (s[19] != '0')
                {
                    tb32.IsReadOnly = true;
                    tb32.Text = s[19].ToString();
                }
                if (s[20] != '0')
                {
                    tb33.IsReadOnly = true;
                    tb33.Text = s[20].ToString();
                }
                if (s[21] != '0')
                {
                    tb34.IsReadOnly = true;
                    tb34.Text = s[21].ToString();
                }
                if (s[22] != '0')
                {
                    tb35.IsReadOnly = true;
                    tb35.Text = s[22].ToString();
                }
                if (s[23] != '0')
                {
                    tb36.IsReadOnly = true;
                    tb36.Text = s[23].ToString();
                }
                if (s[24] != '0')
                {
                    tb37.IsReadOnly = true;
                    tb37.Text = s[24].ToString();
                }
                if (s[25] != '0')
                {
                    tb38.IsReadOnly = true;
                    tb38.Text = s[25].ToString();
                }
                if (s[26] != '0')
                {
                    tb39.IsReadOnly = true;
                    tb39.Text = s[26].ToString();
                }
                if (s[27] != '0')
                {
                    tb41.IsReadOnly = true;
                    tb41.Text = s[27].ToString();
                }
                if (s[28] != '0')
                {
                    tb42.IsReadOnly = true;
                    tb42.Text = s[28].ToString();
                }
                if (s[29] != '0')
                {
                    tb43.IsReadOnly = true;
                    tb43.Text = s[29].ToString();
                }
                if (s[30] != '0')
                {
                    tb44.IsReadOnly = true;
                    tb44.Text = s[30].ToString();
                }
                if (s[31] != '0')
                {
                    tb45.IsReadOnly = true;
                    tb45.Text = s[31].ToString();
                }
                if (s[32] != '0')
                {
                    tb46.IsReadOnly = true;
                    tb46.Text = s[32].ToString();
                }
                if (s[33] != '0')
                {
                    tb47.IsReadOnly = true;
                    tb47.Text = s[33].ToString();
                }
                if (s[34] != '0')
                {
                    tb48.IsReadOnly = true;
                    tb48.Text = s[34].ToString();
                }
                if (s[35] != '0')
                {
                    tb49.IsReadOnly = true;
                    tb49.Text = s[35].ToString();
                }
                if (s[36] != '0')
                {
                    tb51.IsReadOnly = true;
                    tb51.Text = s[36].ToString();
                }
                if (s[37] != '0')
                {
                    tb52.IsReadOnly = true;
                    tb52.Text = s[37].ToString();
                }
                if (s[38] != '0')
                {
                    tb53.IsReadOnly = true;
                    tb53.Text = s[38].ToString();
                }
                if (s[39] != '0')
                {
                    tb54.IsReadOnly = true;
                    tb54.Text = s[39].ToString();
                }
                if (s[40] != '0')
                {
                    tb55.IsReadOnly = true;
                    tb55.Text = s[40].ToString();
                }
                if (s[41] != '0')
                {
                    tb56.IsReadOnly = true;
                    tb56.Text = s[41].ToString();
                }
                if (s[42] != '0')
                {
                    tb57.IsReadOnly = true;
                    tb57.Text = s[42].ToString();
                }
                if (s[43] != '0')
                {
                    tb58.IsReadOnly = true;
                    tb58.Text = s[43].ToString();
                }
                if (s[44] != '0')
                {
                    tb59.IsReadOnly = true;
                    tb59.Text = s[44].ToString();
                }
                if (s[45] != '0')
                {
                    tb61.IsReadOnly = true;
                    tb61.Text = s[45].ToString();
                }
                if (s[46] != '0')
                {
                    tb62.IsReadOnly = true;
                    tb62.Text = s[46].ToString();
                }
                if (s[47] != '0')
                {
                    tb63.IsReadOnly = true;
                    tb63.Text = s[47].ToString();
                }
                if (s[48] != '0')
                {
                    tb64.IsReadOnly = true;
                    tb64.Text = s[48].ToString();
                }
                if (s[49] != '0')
                {
                    tb65.IsReadOnly = true;
                    tb65.Text = s[49].ToString();
                }
                if (s[50] != '0')
                {
                    tb66.IsReadOnly = true;
                    tb66.Text = s[50].ToString();
                }
                if (s[51] != '0')
                {
                    tb67.IsReadOnly = true;
                    tb67.Text = s[51].ToString();
                }
                if (s[52] != '0')
                {
                    tb68.IsReadOnly = true;
                    tb68.Text = s[52].ToString();
                }
                if (s[53] != '0')
                {
                    tb69.IsReadOnly = true;
                    tb69.Text = s[53].ToString();
                }
                if (s[54] != '0')
                {
                    tb71.IsReadOnly = true;
                    tb71.Text = s[54].ToString();
                }
                if (s[55] != '0')
                {
                    tb72.IsReadOnly = true;
                    tb72.Text = s[55].ToString();
                }
                if (s[56] != '0')
                {
                    tb73.IsReadOnly = true;
                    tb73.Text = s[56].ToString();
                }
                if (s[57] != '0')
                {

                    tb74.IsReadOnly = true;
                    tb74.Text = s[57].ToString();
                }
                if (s[58] != '0')
                {
                    tb75.IsReadOnly = true;
                    tb75.Text = s[58].ToString();
                }
                if (s[59] != '0')
                {
                    tb76.IsReadOnly = true;
                    tb76.Text = s[59].ToString();
                }
                if (s[60] != '0')
                {
                    tb77.IsReadOnly = true;
                    tb77.Text = s[60].ToString();
                }
                if (s[61] != '0')
                {
                    tb78.IsReadOnly = true;
                    tb78.Text = s[61].ToString();
                }
                if (s[62] != '0')
                {
                    tb79.IsReadOnly = true;
                    tb79.Text = s[62].ToString();
                }
                if (s[63] != '0')
                {
                    tb81.IsReadOnly = true;
                    tb81.Text = s[63].ToString();
                }
                if (s[64] != '0')
                {
                    tb82.IsReadOnly = true;
                    tb82.Text = s[64].ToString();
                }
                if (s[65] != '0')
                {
                    tb83.IsReadOnly = true;
                    tb83.Text = s[65].ToString();

                }
                if (s[66] != '0')
                {
                    tb84.IsReadOnly = true;
                    tb84.Text = s[66].ToString();
                }
                if (s[67] != '0')
                {
                    tb85.IsReadOnly = true;
                    tb85.Text = s[67].ToString();
                }
                if (s[68] != '0')
                {
                    tb86.IsReadOnly = true;
                    tb86.Text = s[68].ToString();
                }
                if (s[69] != '0')
                {
                    tb87.IsReadOnly = true;
                    tb87.Text = s[69].ToString();
                }
                if (s[70] != '0')
                {
                    tb88.IsReadOnly = true;
                    tb88.Text = s[70].ToString();
                }
                if (s[71] != '0')
                {
                    tb89.IsReadOnly = true;
                    tb89.Text = s[71].ToString();
                }
                if (s[72] != '0')
                {
                    tb91.IsReadOnly = true;
                    tb91.Text = s[72].ToString();
                }
                if (s[73] != '0')
                {
                    tb92.IsReadOnly = true;
                    tb92.Text = s[73].ToString();
                }
                if (s[74] != '0')
                {
                    tb93.IsReadOnly = true;
                    tb93.Text = s[74].ToString();
                }
                if (s[75] != '0')
                {
                    tb94.IsReadOnly = true;
                    tb94.Text = s[75].ToString();
                }
                if (s[76] != '0')
                {
                    tb95.IsReadOnly = true;
                    tb95.Text = s[76].ToString();
                }
                if (s[77] != '0')
                {
                    tb96.IsReadOnly = true;
                    tb96.Text = s[77].ToString();
                }
                if (s[78] != '0')
                {
                    tb97.IsReadOnly = true;
                    tb97.Text = s[78].ToString();
                }
                if (s[79] != '0')
                {
                    tb98.IsReadOnly = true;
                    tb98.Text = s[79].ToString();
                }
                if (s[80] != '0')
                {
                    tb99.IsReadOnly = true;
                    tb99.Text = s[80].ToString();
                }
            }
        
    }

            private void btnStop_Click(object sender, RoutedEventArgs e)
            {
            /// </summary>
            ///Przycisk czyszczący planszę sudku
            /// </summary>
                File.Delete(@"sudokuPelne.txt");
                File.Delete(@"sudokuPuste.txt");


                clear();
             }

            private void btnRozwiazanie_Click(object sender, RoutedEventArgs e)
            {
            /// </summary>
            ///przycisk pokazujący rozwiązanie dla danej rozgrywki
            /// </summary>
            if (File.Exists(@"sudokuPelne.txt"))
            {
                string line1 = File.ReadAllText(@"sudokuPelne.txt");
                var ln = line1;
                char[] s = ln.ToCharArray();

                

                clear();

                if (s[0] != '0')
                {
                    tb11.IsReadOnly = true;
                    tb11.Text = s[0].ToString();
                }
                if (s[1] != '0')
                {
                    tb12.IsReadOnly = true;
                    tb12.Text = s[1].ToString();
                }
                if (s[2] != '0')
                {
                    tb13.IsReadOnly = true;
                    tb13.Text = s[2].ToString();
                }
                if (s[3] != '0')
                {
                    tb14.IsReadOnly = true;
                    tb14.Text = s[3].ToString();
                }
                if (s[4] != '0')
                {
                    tb15.IsReadOnly = true;
                    tb15.Text = s[4].ToString();
                }
                if (s[5] != '0')
                {
                    tb16.IsReadOnly = true;
                    tb16.Text = s[5].ToString();
                }
                if (s[6] != '0')
                {
                    tb17.IsReadOnly = true;
                    tb17.Text = s[6].ToString();
                }
                if (s[7] != '0')
                {
                    tb18.IsReadOnly = true;
                    tb18.Text = s[7].ToString();
                }
                if (s[8] != '0')
                {
                    tb19.IsReadOnly = true;
                    tb19.Text = s[8].ToString();
                }
                if (s[9] != '0')
                {
                    tb21.IsReadOnly = true;
                    tb21.Text = s[9].ToString();
                }
                if (s[10] != '0')
                {
                    tb22.IsReadOnly = true;
                    tb22.Text = s[10].ToString();

                }
                if (s[11] != '0')
                {
                    tb23.IsReadOnly = true;
                    tb23.Text = s[11].ToString();
                }
                if (s[12] != '0')
                {
                    tb24.IsReadOnly = true;
                    tb24.Text = s[12].ToString();
                }
                if (s[13] != '0')
                {
                    tb25.IsReadOnly = true;
                    tb25.Text = s[13].ToString();
                }
                if (s[14] != '0')
                {
                    tb26.IsReadOnly = true;
                    tb26.Text = s[14].ToString();
                }
                if (s[15] != '0')
                {
                    tb27.IsReadOnly = true;
                    tb27.Text = s[15].ToString();
                }
                if (s[16] != '0')
                {
                    tb28.IsReadOnly = true;
                    tb28.Text = s[16].ToString();
                }
                if (s[17] != '0')
                {
                    tb29.IsReadOnly = true;
                    tb29.Text = s[17].ToString();
                }
                if (s[18] != '0')
                {
                    tb31.IsReadOnly = true;
                    tb31.Text = s[18].ToString();
                }
                if (s[19] != '0')
                {
                    tb32.IsReadOnly = true;
                    tb32.Text = s[19].ToString();
                }
                if (s[20] != '0')
                {
                    tb33.IsReadOnly = true;
                    tb33.Text = s[20].ToString();
                }
                if (s[21] != '0')
                {
                    tb34.IsReadOnly = true;
                    tb34.Text = s[21].ToString();
                }
                if (s[22] != '0')
                {
                    tb35.IsReadOnly = true;
                    tb35.Text = s[22].ToString();
                }
                if (s[23] != '0')
                {
                    tb36.IsReadOnly = true;
                    tb36.Text = s[23].ToString();
                }
                if (s[24] != '0')
                {
                    tb37.IsReadOnly = true;
                    tb37.Text = s[24].ToString();
                }
                if (s[25] != '0')
                {
                    tb38.IsReadOnly = true;
                    tb38.Text = s[25].ToString();
                }
                if (s[26] != '0')
                {
                    tb39.IsReadOnly = true;
                    tb39.Text = s[26].ToString();
                }
                if (s[27] != '0')
                {
                    tb41.IsReadOnly = true;
                    tb41.Text = s[27].ToString();
                }
                if (s[28] != '0')
                {
                    tb42.IsReadOnly = true;
                    tb42.Text = s[28].ToString();
                }
                if (s[29] != '0')
                {
                    tb43.IsReadOnly = true;
                    tb43.Text = s[29].ToString();
                }
                if (s[30] != '0')
                {
                    tb44.IsReadOnly = true;
                    tb44.Text = s[30].ToString();
                }
                if (s[31] != '0')
                {
                    tb45.IsReadOnly = true;
                    tb45.Text = s[31].ToString();
                }
                if (s[32] != '0')
                {
                    tb46.IsReadOnly = true;
                    tb46.Text = s[32].ToString();
                }
                if (s[33] != '0')
                {
                    tb47.IsReadOnly = true;
                    tb47.Text = s[33].ToString();
                }
                if (s[34] != '0')
                {
                    tb48.IsReadOnly = true;
                    tb48.Text = s[34].ToString();
                }
                if (s[35] != '0')
                {
                    tb49.IsReadOnly = true;
                    tb49.Text = s[35].ToString();
                }
                if (s[36] != '0')
                {
                    tb51.IsReadOnly = true;
                    tb51.Text = s[36].ToString();
                }
                if (s[37] != '0')
                {
                    tb52.IsReadOnly = true;
                    tb52.Text = s[37].ToString();
                }
                if (s[38] != '0')
                {
                    tb53.IsReadOnly = true;
                    tb53.Text = s[38].ToString();
                }
                if (s[39] != '0')
                {
                    tb54.IsReadOnly = true;
                    tb54.Text = s[39].ToString();
                }
                if (s[40] != '0')
                {
                    tb55.IsReadOnly = true;
                    tb55.Text = s[40].ToString();
                }
                if (s[41] != '0')
                {
                    tb56.IsReadOnly = true;
                    tb56.Text = s[41].ToString();
                }
                if (s[42] != '0')
                {
                    tb57.IsReadOnly = true;
                    tb57.Text = s[42].ToString();
                }
                if (s[43] != '0')
                {
                    tb58.IsReadOnly = true;
                    tb58.Text = s[43].ToString();
                }
                if (s[44] != '0')
                {
                    tb59.IsReadOnly = true;
                    tb59.Text = s[44].ToString();
                }
                if (s[45] != '0')
                {
                    tb61.IsReadOnly = true;
                    tb61.Text = s[45].ToString();
                }
                if (s[46] != '0')
                {
                    tb62.IsReadOnly = true;
                    tb62.Text = s[46].ToString();
                }
                if (s[47] != '0')
                {
                    tb63.IsReadOnly = true;
                    tb63.Text = s[47].ToString();
                }
                if (s[48] != '0')
                {
                    tb64.IsReadOnly = true;
                    tb64.Text = s[48].ToString();
                }
                if (s[49] != '0')
                {
                    tb65.IsReadOnly = true;
                    tb65.Text = s[49].ToString();
                }
                if (s[50] != '0')
                {
                    tb66.IsReadOnly = true;
                    tb66.Text = s[50].ToString();
                }
                if (s[51] != '0')
                {
                    tb67.IsReadOnly = true;
                    tb67.Text = s[51].ToString();
                }
                if (s[52] != '0')
                {
                    tb68.IsReadOnly = true;
                    tb68.Text = s[52].ToString();
                }
                if (s[53] != '0')
                {
                    tb69.IsReadOnly = true;
                    tb69.Text = s[53].ToString();
                }
                if (s[54] != '0')
                {
                    tb71.IsReadOnly = true;
                    tb71.Text = s[54].ToString();
                }
                if (s[55] != '0')
                {
                    tb72.IsReadOnly = true;
                    tb72.Text = s[55].ToString();
                }
                if (s[56] != '0')
                {
                    tb73.IsReadOnly = true;
                    tb73.Text = s[56].ToString();
                }
                if (s[57] != '0')
                {

                    tb74.IsReadOnly = true;
                    tb74.Text = s[57].ToString();
                }
                if (s[58] != '0')
                {
                    tb75.IsReadOnly = true;
                    tb75.Text = s[58].ToString();
                }
                if (s[59] != '0')
                {
                    tb76.IsReadOnly = true;
                    tb76.Text = s[59].ToString();
                }
                if (s[60] != '0')
                {
                    tb77.IsReadOnly = true;
                    tb77.Text = s[60].ToString();
                }
                if (s[61] != '0')
                {
                    tb78.IsReadOnly = true;
                    tb78.Text = s[61].ToString();
                }
                if (s[62] != '0')
                {
                    tb79.IsReadOnly = true;
                    tb79.Text = s[62].ToString();
                }
                if (s[63] != '0')
                {
                    tb81.IsReadOnly = true;
                    tb81.Text = s[63].ToString();
                }
                if (s[64] != '0')
                {
                    tb82.IsReadOnly = true;
                    tb82.Text = s[64].ToString();
                }
                if (s[65] != '0')
                {
                    tb83.IsReadOnly = true;
                    tb83.Text = s[65].ToString();

                }
                if (s[66] != '0')
                {
                    tb84.IsReadOnly = true;
                    tb84.Text = s[66].ToString();
                }
                if (s[67] != '0')
                {
                    tb85.IsReadOnly = true;
                    tb85.Text = s[67].ToString();
                }
                if (s[68] != '0')
                {
                    tb86.IsReadOnly = true;
                    tb86.Text = s[68].ToString();
                }
                if (s[69] != '0')
                {
                    tb87.IsReadOnly = true;
                    tb87.Text = s[69].ToString();
                }
                if (s[70] != '0')
                {
                    tb88.IsReadOnly = true;
                    tb88.Text = s[70].ToString();
                }
                if (s[71] != '0')
                {
                    tb89.IsReadOnly = true;
                    tb89.Text = s[71].ToString();
                }
                if (s[72] != '0')
                {
                    tb91.IsReadOnly = true;
                    tb91.Text = s[72].ToString();
                }
                if (s[73] != '0')
                {
                    tb92.IsReadOnly = true;
                    tb92.Text = s[73].ToString();
                }
                if (s[74] != '0')
                {
                    tb93.IsReadOnly = true;
                    tb93.Text = s[74].ToString();
                }
                if (s[75] != '0')
                {
                    tb94.IsReadOnly = true;
                    tb94.Text = s[75].ToString();
                }
                if (s[76] != '0')
                {
                    tb95.IsReadOnly = true;
                    tb95.Text = s[76].ToString();
                }
                if (s[77] != '0')
                {
                    tb96.IsReadOnly = true;
                    tb96.Text = s[77].ToString();
                }
                if (s[78] != '0')
                {
                    tb97.IsReadOnly = true;
                    tb97.Text = s[78].ToString();
                }
                if (s[79] != '0')
                {
                    tb98.IsReadOnly = true;
                    tb98.Text = s[79].ToString();
                }
                if (s[80] != '0')
                {
                    tb99.IsReadOnly = true;
                    tb99.Text = s[80].ToString();
                }
                File.Delete("sudokuPuste.txt");
                File.Delete("sudokuPelne.txt");
            }
            else
            {
                MessageBox.Show("Rozpocznij grę");
            }
             }

            private void btnSprawdz_Click_1(object sender, RoutedEventArgs e)
            {
            /// </summary>
            ///Przycisk sprawdzający warunki wygranej
            /// </summary>
            wygrana();
             }

            private void btnZakoncz_Click(object sender, RoutedEventArgs e)
            {
            /// </summary>
            ///Zamykanie aplikacji
             /// </summary>
            if (File.Exists("sudokuPelne.txt"))
            {
                File.Delete("sudokuPelne.txt");

            }
            if (File.Exists("sudokuPuste.txt"))
            {
                File.Delete("sudokuPuste.txt");
            }
            Close();
        }

        private void tb11_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void tb12_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void tb13_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void tb14_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void tb15_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void tb16_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void rbLatwy_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbSredni_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbTrudny_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
    }

