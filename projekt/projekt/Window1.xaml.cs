﻿using System.IO;
using System.Windows;

namespace projekt
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }
        private void mainwin()
        {
            /// </summary>
            ///funkcja otwierająca nowe ookno
            /// </summary>
            MainWindow subWindow = new MainWindow();
            subWindow.Show();
        }
        
        private void btnZamknij_Click(object sender, RoutedEventArgs e)
        {
            /// </summary>
            ///Zamykanie aplikacji
             /// </summary>
            Close();
        }

        private void btGraj_Click_1(object sender, RoutedEventArgs e)
        {
            /// </summary>
            ///Usuwanie plików jeśli istnieją
             /// </summary>
            if (File.Exists("sudokuPelne.txt"))
            {
                File.Delete("sudokuPelne.txt");

            }
            if (File.Exists("sudokuPuste.txt"))
            {
                File.Delete("sudokuPuste.txt");
            }
            /// </summary>
            ///Funkcja uruchamiająca otwarcie nowego okna
             /// </summary>
            mainwin();
            /// </summary>
            ///Zamykanie aplikacji
             /// </summary>
            Close();
        }
    }
}
